//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
    let output = "" //empty output, fill this so that it can print onto the page.
    
    //Question 1 here 
    let n = [54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19, -51, -17, -25]
    let posOdd = []
    let negEven = []
    
    for(let i = 0; i < n.length; i++){
        var mod = n[i] % 2
        if(n[i] > 0 && mod != 0)
            posOdd.push(n[i])
        else if(n[i] < 0 && mod === 0)
            negEven.push(n[i])
    }
    output += "Positive Odd: "
    output += posOdd + "\n"
    output += "Negative Odd: "
    output += negEven
    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}

function question2(){
    let output = "" 
    
    //Question 2 here 
    let f1=0;f2=0;f3=0;f4=0;f5=0;f6=0;
    for(i=0; i <= 60000; i++){
    let roll = Math.floor((Math.random() * 6) + 1)
        if(roll === 1){
            f1++
        }
        else if(roll === 2){
            f2++
        }
        else if(roll === 2){
            f2++
        }
        else if(roll === 3){
            f3++
        }
        else if(roll === 4){
            f4++
        }
        else if(roll === 5){
            f5++
        }
        else if(roll === 6){
            f6++
        }
        
    }
    output += "1: " + f1 + "\n"
    output += "2: " + f2 + "\n"
    output += "3: " + f3 + "\n"
    output += "4: " + f4 + "\n"
    output += "5: " + f5 + "\n"
    output += "6: " + f6
    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}

function question3(){
    let output = "" 
    
    //Question 3 here 
    let index = [0,0,0,0,0,0,0]
    for(i=0; i <= 60000; i++){
        let roll = Math.floor((Math.random() * 6) + 1)
        index[roll]++
    }
    output += "1: " + index[1] + "\n"
    output += "2: " + index[2] + "\n"
    output += "3: " + index[3] + "\n"
    output += "4: " + index[4] + "\n"
    output += "5: " + index[5] + "\n"
    output += "6: " + index[6] + "\n"
    
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}

function question4(){
    let output = "" 
    
    //Question 4 here 
    var rolls = {
        amounts:{
            1:0,
            2:0,
            3:0,
            4:0,
            5:0,
            6:0,
        },
        total: 60000,
        exception: "",
    }
    
    for(i=0; i <= rolls.total; i++){
        let roll = Math.floor((Math.random() * 6) + 1);
        rolls.amounts.roll++
    }
    
    for(let prop in rolls.amounts){
        if(rolls.amounts[prop] > 10100 || rolls.amounts[prop] > 9900)
            rolls.exception += prop
    }
    
    output += rolls.exception + "gg"
    
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}

function question5(){
    let output = "" 
    
    //Question 5 here 
    let person = {
        name: "Jane",
        income: 127050
    }
    let tax = 0
    if(person.income <=18200){
         tax = 0
    }
    else if(person.income <=37000){
         tax = 0.19*(person.income-18200)
    }
        
    else if(person.income <=90000){
         tax = 3572 +0.325*(person.income-37000)
    }
        
    else if(person.income <=180000){
         tax = 20797 + 0.37*(person.income-90000)
    }
        
    else if(person.income > 180000){
         tax = 54097 + 0.45*(person.income-180000)
    }
    output += person.name
    output += "'s income is: "
    output += "$" + person.income + ", and her"+ "\n"
    output += "tax is: "
    output += "$" + tax


    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}